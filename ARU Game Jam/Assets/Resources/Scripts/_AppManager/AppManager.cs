﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Assets.ScriptableObjects;
using Assets.Scripts.Background;

namespace Assets.Scripts._AppManager
{
    public class AppManager : MonoBehaviour
    {
        public so_ActionText actionText;

        public GameObject bgTrainPanel;
        public GameObject fgTrainPanel;

        private GameObject _bgCanvas;
        private GameObject _bgContainer;
        [SerializeField]
        private int _bgCanvasStart;

        private GameObject _fgCanvas;
        private GameObject _fgContainer;
        [SerializeField]
        private int _fgCanvasStart;

        private List<GameObject> _backgroundTiles;
        private List<GameObject> _foregroundTiles;

        private void Awake()
        {
            _bgCanvas = GameObject.Find("BackgroundCanvas");
            _bgContainer = GameObject.Find("BackgroundContainer");
            _bgCanvasStart = Mathf.FloorToInt(_bgCanvas.transform.position.x);

            _fgCanvas = GameObject.Find("ForegroundCanvas");
            _fgContainer = GameObject.Find("ForegroundContainer");
            _fgCanvasStart = Mathf.FloorToInt(_fgCanvas.transform.position.x);

            _backgroundTiles = new List<GameObject>();
            _foregroundTiles = new List<GameObject>();
        }

        private void Start()
        {
            // ALL UI NEEDS TO HAVE CONTAINER AS A PARENT IF IT'S IN THE LAYOUT GROUP

            for (int i = 0; i < 6; i++)
            {
                var go = Instantiate(bgTrainPanel, _bgContainer.transform, false);
                _backgroundTiles.Add(go);
            }
            for (int i = 0; i < 6; i++)
            {
                var go = Instantiate(fgTrainPanel, _fgContainer.transform, false);
                _foregroundTiles.Add(go);
            }
        }

        private void Update()
        {
            //if(((Mathf.FloorToInt(_bgContainer.transform.position.x) % 480) < 5))
            //{
            //    var go = Instantiate(bgTrainPanel, _bgContainer.transform, false);
            //    _backgroundTiles.Add(go);

            //    Destroy(_backgroundTiles[0]);
            //    _backgroundTiles.RemoveAt(0);
            //}

            //if (((Mathf.FloorToInt(_fgContainer.transform.position.x) % 480) < 5))
            //{
            //    var go = Instantiate(fgTrainPanel, _fgContainer.transform, false);
            //    _foregroundTiles.Add(go);

            //    Destroy(_foregroundTiles[0]);
            //    _foregroundTiles.RemoveAt(0);
            //}
        }
    }
}
