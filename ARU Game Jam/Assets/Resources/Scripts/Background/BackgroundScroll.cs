﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Background
{
    public class BackgroundScroll : MonoBehaviour
    {
        [SerializeField]
        private float _scrollSpeed = 10f;

        public int scrollCount;

        public void Update()
        {
            Scroll();
            scrollCount++;
        }

        private void Scroll()
        {
            gameObject.transform.Translate(new Vector3(-_scrollSpeed * Time.timeScale, 0, 0));

        }
    }
}
