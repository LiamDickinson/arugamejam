﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class EventParse : MonoBehaviour
{
    private Text eventText;
    public TextAsset eventTextDoc;
    private string[] eventTextLines;
    public static int eventNumber = -1;
   
    private void Start()
    {      
        eventText = GameObject.Find("Event_Text").GetComponent<Text>();

        if(eventTextDoc != null) 
        {
            eventTextLines = (eventTextDoc.text.Split('\n')); //Fills the array with the text from the text document
        }
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            //int eventNumber = Random.Range(0, eventTextLines.Length);
            eventNumber++;
            DisplayText(eventNumber);
        }
    }

    /// <summary>
    /// Runs this method with the eventNumber
    /// </summary>
    /// <param name="eventNumber"></param>
    public void DisplayText(int eventNumber)
    {
        eventText.text = eventTextLines[eventNumber];
    }
}