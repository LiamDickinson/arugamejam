﻿using UnityEngine;

public class EventAudio : MonoBehaviour
{
    public AudioClip[] eventAudio;
    public AudioSource audioSource;

    void playEventAudio(int eventNumber)
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = eventAudio[eventNumber];
        audioSource.Play();
    }
}