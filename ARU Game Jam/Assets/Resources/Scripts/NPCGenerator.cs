﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NPCGenerator : MonoBehaviour
{
    [SerializeField]
    List<Sprite> headsList = new List<Sprite>();
    [SerializeField]
    List<Sprite> hairList = new List<Sprite>();
    [SerializeField]
    List<Sprite> topList = new List<Sprite>();

    public Image head;
    public Image hair;
    public Image top;

    public Sprite headSelected;
    public Sprite hairSelected;
    public Sprite topSelected;

    void Start()
    {
        ChooseParts();
    }

    void ChooseParts()
    {
        headSelected = headsList[Random.Range(0, 3)];
        hairSelected = hairList[Random.Range(0, 3)];
        topSelected = topList[Random.Range(0, 3)];

        head.GetComponent<Image>().sprite = headSelected;
        hair.GetComponent<Image>().sprite = hairSelected;
        top.GetComponent<Image>().sprite = topSelected;
    }
}