﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QTEKeyGen : MonoBehaviour {

    public List<Sprite> keyImagesList = new List<Sprite>();
    private Image keyImage;
    private Transform imageTransform;
    private float randomX, randomY;

    private void Start()
    {
        keyImage = GameObject.Find("QTE_Key").GetComponent<Image>();
        imageTransform = keyImage.transform;
        PickKey();
        MoveKeyImage();
    }

    void PickKey()
    {
        keyImage.sprite = keyImagesList[Random.Range(0, keyImagesList.Count)];
    }

    void MoveKeyImage()
    {
        if(keyImage.enabled == false)
        {
            keyImage.enabled = true;
        }

        randomX = Random.Range(-20, 20);
        randomY = Random.Range(-20, 20);
      
    }

    private void Update()
    {
        imageTransform.position += new Vector3(randomX, randomY, 0) * Time.deltaTime;
    }

}
